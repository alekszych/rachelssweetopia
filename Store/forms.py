from django import forms
from .models import *


class ImageForm(forms.ModelForm):
    class Meta:
        model = Photo
        fields = ["image", "product", "order"]
        widgets = {
            "image": forms.FileInput(attrs={'id': 'image-form'}),
            "product": forms.TextInput
        }


class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = ["title", "description", "icon"]
        widgets = {
            "title": forms.TextInput(attrs={'class': 'product-title-form'}),
            "description": forms.TextInput(attrs={'class': 'product-description-form'}),
            "icon": forms.FileInput(attrs={'class': 'product-icon-form'}),
        }
