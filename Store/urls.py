from django.urls import path
from . import views


urlpatterns = [
    path('', views.home, name="home"),
    path('catalog', views.catalog, name="catalog"),
    path('about', views.about, name="about"),
    path('panel', views.admin, name='panel'),
    path('contact', views.contact, name="contact"),
]