# Generated by Django 3.1.4 on 2020-12-20 19:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Store', '0007_auto_20201220_2002'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='icon',
            field=models.FileField(default='images/product_icons/macaroons.svg', upload_to='product_icons'),
        ),
    ]
