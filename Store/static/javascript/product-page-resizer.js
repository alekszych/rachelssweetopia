let pageResizer = () => {
    let image = document.getElementById('product-image');
    if ((window.innerWidth / window.innerHeight) < 2 && window.innerWidth > window.innerHeight){
        let newHeight = (window.innerWidth / 3.1).toString()
        image.style.height = "0px".replace("0", newHeight)
    }
    else {
        image.style.height = '65vh'
    }
}