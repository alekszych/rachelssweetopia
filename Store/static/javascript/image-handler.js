let imageHandler = (photoAmount, title, photoNumber) => {
    let photoSelector = document.getElementById('photo-selector');
    if(photoAmount !== 1){
        for (let i = 0; i < photoAmount; i++){
            let number = document.createElement("a");
            number.className = "number";
            number.innerText = (i+1).toString();
            let href = "catalog?title=PRODUCT&photo="+i;
            number.href = href.replace("PRODUCT", title);
            photoSelector.appendChild(number)
            if(i === (photoNumber) && i !== (photoAmount-1)){
                let line = document.createElement("div")
                line.className = "line";
                photoSelector.appendChild(line);
            }
        }
    }
}