let counter = 1

let toggleNav = () => {
    let list = document.getElementById('product-list');
    if (counter % 2 === 0)
        list.style.visibility = 'hidden';
    else
        list.style.visibility = 'visible';
    counter++;
}