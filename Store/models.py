from django.db import models

# Create your models here.


class Product(models.Model):
    title = models.TextField(null=True)
    description = models.TextField(null=True)
    icon = models.FileField(upload_to='product_icons', default='product_icons/macaroons.svg')

    def __str__(self):
        return self.title


class Photo(models.Model):
    image = models.ImageField(upload_to='product_photos')
    product = models.ForeignKey(Product, null=True, on_delete=models.CASCADE)
    order = models.IntegerField(null=True, default=10)

    def __str__(self):
        return self.product.title+" "+str(self.order)
