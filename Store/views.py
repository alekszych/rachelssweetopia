from django.http import QueryDict
from django.shortcuts import render
from .decorators import *
from .forms import *
from django.core import mail
from django.template.loader import render_to_string
from django.utils.html import strip_tags


# Create your views here.


def home(request):
    products = Product.objects.all()
    dictionary = {'products': products}
    return render(request, 'home_page.html', dictionary)


def catalog(request):
    products = Product.objects.all()
    if request.method == 'GET' and 'title' in request.GET:
        productTitle = request.GET['title']
        product = Product.objects.filter(title=productTitle)[0]
        photos = Photo.objects.filter(product__title=productTitle).order_by('order')
        photoAmount = photos.count()
        choice = True
        if 'photo' in request.GET:
            photoNumber = request.GET["photo"]
        else:
            photoNumber = str(0)
        photoPath = "photos[" + photoNumber + "].image.url"
        photoPath = eval(photoPath)
        dictionary = {
            'photos': photos,
            'product': product,
            'products': products,
            'choice': choice,
            'photoAmount': photoAmount,
            'photoNumber': photoNumber,
            'photoPath': photoPath,
        }
    else:
        List = []
        for product in products:
            photo = Photo.objects.filter(product=product).order_by('order')[0]
            photo = photo.image
            List.append({"product": product.title, "photo": str(photo)})
        choice = False
        dictionary = {
            'list': List,
            'products': products,
            'choice': choice
        }
    return render(request, 'catalog_page.html', dictionary)


def about(request):
    products = Product.objects.all()
    dictionary = {'products': products}
    return render(request, 'about_page.html', dictionary)


@allowed_users("staff")
def admin(request):
    # Image saving
    if request.method == 'POST' and 'image' in request.FILES:
        post = QueryDict('product=' + str(Product.objects.get(title=request.GET['title']).pk), mutable=True)
        post.update(request.POST)
        form = ImageForm(post, request.FILES)
        if form.is_valid():
            form.save()
            redirect("panel")
    # Image deleting
    if request.method == 'GET' and 'image' in request.GET:
        image = Photo.objects.get(pk=request.GET['image'])
        image.delete()
    # Product data editing
    if request.method == 'POST' and 'product-title' in request.POST and 'product-description' in request.POST:
        product = Product.objects.get(title=request.GET['title'])
        product.title = request.POST['product-title']
        product.description = request.POST['product-description']
        if request.FILES.get('product-icon', False):
            product.icon = request.FILES['product-icon']
        product.save()
    # Display
    choice = False
    products = Product.objects.all()
    # Product creation
    productForm = ProductForm(request.POST, request.FILES)
    if productForm.is_valid():
        print(productForm)
        productForm.save()
    # Product deletion
    if request.method == 'POST' and "delete-product" in request.POST:
        product = Product.objects.get(title=request.GET['title'])
        product.delete()
    # Display
    if request.method == 'GET' and 'title' in request.GET:
        choice = True
        product = Product.objects.get(title=request.GET['title'])
        productTitle = request.GET['title']
        photos = Photo.objects.filter(product__title=request.GET["title"]).order_by('order')

        dictionary = {
            "choice": choice,
            "product": product,
            "products": products,
            "photos": photos,
            "imageForm": ImageForm,
            'productTitle': productTitle,
        }
    else:
        dictionary = {
            "products": products,
            "choice": choice,
            "productForm": productForm,
        }
    return render(request, 'admin_page.html', dictionary)


def contact(request):
    products = Product.objects.all()
    if request.method == 'POST' and 'message' in request.POST:
        subject = "Sweetopia: "+request.POST['type']
        html_message = render_to_string('mail_template.html', {
            'email': request.POST['email'],
            'firstName': request.POST['firstName'],
            'lastName': request.POST['lastName'],
            'phone': request.POST['phone'],
            'message': request.POST['message'],
        })
        plain_message = strip_tags(html_message)
        from_email = 'RachelSweetopia <alekszych2004@gmail.com>'
        to = 'vjasieg@gmail.com'
        mail.send_mail(subject, plain_message, from_email, [to], html_message=html_message)
    dictionary = {'products': products}
    return render(request, 'contact_page.html', dictionary)
